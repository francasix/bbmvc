define([
	'jquery',
	'backbone',
	'views/userlistView',
], function ($, Backbone,userList) {
	'use strict';

	var User = Backbone.Model.extend({
		urlRoot: '/users',
		initialize: function(){
			
		},
	});
	return User;
});