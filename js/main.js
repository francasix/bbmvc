/*global require*/
'use strict';

// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		backboneLocalstorage: {
			deps: ['backbone'],
			exports: 'Store'
		},
		handlebars: {
			exports: 'Handlebars'
		}
	},
	paths: {
		jquery: '../node_modules/jquery/dist/jquery',
		underscore: '../node_modules/underscore/underscore',
		backbone: '../node_modules/backbone/backbone',
		handlebars: '../node_modules/handlebars/dist/handlebars',
	}
});

require([
	'backbone',
	'views/userlistView',
	'models/userModel',
	'routers/router'
], function (Backbone, UserList, Model, Workspace) {
	//each ajax call
    $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
        options.url = 'http://local.bbmvc' + options.url;
    });
	// Initialize routing and start Backbone.history()
	new Workspace();
	Backbone.history.start();

	new Model();

	// Initialize the application view
	new UserList();

});