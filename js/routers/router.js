define([
	'jquery',
	'backbone',
	'views/userlistView',
], function ($, Backbone,userList) {
	'use strict';

	var Router = Backbone.Router.extend({
        routes: {
            '': 'home',
            'new': 'editUser',
            'edit/:id': 'editUser',
            'delete/:id': 'deleteUser'
        },
        home: function(){
			this.view = new userList();
        },
        
    });
	return Router;
});