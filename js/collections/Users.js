define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var Users = Backbone.Collection.extend({
		url: '/users',
		initialize: function(){
			
		}
	});
	
	return Users;

});