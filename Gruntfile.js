module.exports = function(grunt) {


  // Project configuration.
  grunt.initConfig({
    config : grunt.file.readJSON('config.json'),
    pkg: grunt.file.readJSON('package.json'),
    handlebars: {
      compile: {
        options: {
          amd: true,
          namespace: "tpl"
        },
        files: {
          '<%= config.precompiled_path %>/tpl.js': '<%= config.hbs_path %>/*.hbs'
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: '<%= config.js_path %>',
        dest: '<%= config.js_path_dest %>global.js'
      }
    },
    watch: {
      scripts: {
        files: ['<%= config.js_path %>','<%= config.hbs_path %>/*.hbs'],
        tasks: ['uglify','handlebars'],
          options: {
            spawn: false,
          }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-handlebars');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('dev', ['handlebars','uglify','watch']);

  grunt.registerTask('prod', ['uglify','handlebars']);

};