define(['handlebars'], function(Handlebars) {

this["tpl"] = this["tpl"] || {};

this["tpl"]["js/templates/userListTpl.hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<tr>\r\n			<td>\r\n				"
    + alias4(((helper = (helper = helpers.firstname || (depth0 != null ? depth0.firstname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstname","hash":{},"data":data}) : helper)))
    + "\r\n			</td>\r\n			<td>\r\n				"
    + alias4(((helper = (helper = helpers.lastname || (depth0 != null ? depth0.lastname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lastname","hash":{},"data":data}) : helper)))
    + "\r\n			</td>\r\n			<td>\r\n				"
    + alias4(((helper = (helper = helpers.age || (depth0 != null ? depth0.age : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"age","hash":{},"data":data}) : helper)))
    + "\r\n			</td>\r\n			<td>\r\n				<a href=\"#/edit/"
    + alias4(container.lambda(depth0, depth0))
    + "\">edit</a>\r\n			</td>\r\n			<td>\r\n				<a class=\"delete\" href=\"\" data-id=\""
    + alias4(((helper = (helper = helpers.id || (data && data.id)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">delete</a>\r\n			</td>\r\n		</tr>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div id=\"user-list-tpl\">\r\n<a href=\"#/new\">New user</a>\r\n	<table>\r\n		<thead>\r\n			<tr>\r\n				<th>firstname</th>\r\n				<th>lastname</th>\r\n				<th>age</th>\r\n			</tr>\r\n	</thead>\r\n	<tbody>\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.users : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</tbody>\r\n	</table>\r\n	</div>";
},"useData":true});

return this["tpl"];

});