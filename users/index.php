<?php

    $entity = array(
    		0 => array(
    		    "firstname" => 'Pope',
	            "lastname"  => "Toto",
	            "age"       => 21
    		),
        	1 => array(
    		    "firstname" => 'Jean-Paul',
	            "lastname"  => "II",
	            "age"       => 101
        	)
    );

    echo json_encode($entity);